package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    static int [] CreateMas()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input n: ");
        int n = scanner.nextInt();

        return new int[n];
    }

    static void FillMasRandom(int[] mas)
    {
        Random rnd = new Random();

        for (int i=0; i<mas.length;i++)
        {
            mas[i]=rnd.nextInt(100);
        }
    }

    static void PrintMas(int[] mas)
    {
        for (int i=0; i<mas.length;i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.println();
    }

    static int SumMas(int[] mas)
    {
        int sum=0;

        for (int i=0; i<mas.length;i++)
        {
            sum+=mas[i];
        }
        return sum;
    }

    static int MulMas(int[] mas)
    {
        int mul=1;

        for (int i=0; i<mas.length;i++)
        {
            mul*=mas[i];
        }
        return mul;
    }

    static int MinMas(int[] mas)
    {
        int min = mas[0];

        for (int i=0; i<mas.length;i++)
        {
            if(mas[i]<min)
            {
                min=mas[i];
            }
        }
        return min;
    }

    static int MaxMas(int[] mas)
    {
        int max = mas[0];

        for (int i=0; i<mas.length;i++)
        {
            if(mas[i]>max)
            {
                max=mas[i];
            }
        }
        return max;
    }

    static int FindIndex(int[] mas)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input k: ");
        int k=scanner.nextInt();

        for (int i=0; i<mas.length;i++)
        {
            if(mas[i]==k)
            {
                return mas[i];
            }
        }
        return -1;
    }

    static void IncreaseMas(int[] mas)
    {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Input k: ");
        int k=scanner.nextInt();

        for (int i=0; i<mas.length;i++)
        {
            mas[i]*=k;
        }
    }

    static void ReverseMas(int[] mas)
    {
        int temp;

        for (int i=0; i<mas.length/2;i++)
        {
            temp=mas[i];
            mas[i]=mas[mas.length-1-i];
            mas[mas.length-1-i]=temp;
        }
    }

    public static void main(String[] args)
    {
        int[] mas = CreateMas();
        FillMasRandom(mas);
        PrintMas(mas);

        //1
        /*int sum = SumMas(mas);
        System.out.println(sum);
        PrintMas(mas);*/

        //2
        /*int mul = MulMas(mas);
        System.out.println(mul);
        PrintMas(mas);*/

        //3
        /*int min = MinMas(mas);
        System.out.println(min);
        PrintMas(mas);*/

        //4
        /*int max = MaxMas(mas);
        System.out.println(max);
        PrintMas(mas);*/

        //5
        /*int findIndex = FindIndex(mas);
        System.out.println(findIndex);
        PrintMas(mas);*/

        //6
        /*IncreaseMas(mas);
        PrintMas(mas);*/

        //7
        ReverseMas(mas);
        PrintMas(mas);
    }
}